import React, { Component } from 'react';

class BookItem extends Component {

  deleteBook(id){
    this.props.onDelete(id);
  }

  editBook(id){
    this.props.onEdit(id);
  }

  render() {
    return (
      <li className="Book">
        <strong>Tytuł:</strong> {this.props.book.title} 
        <strong>Gatunek:</strong> {this.props.book.category} 
        <button onClick={this.deleteBook.bind(this, this.props.book.id)}>X</button>
        <button onClick={this.editBook.bind(this, this.props.id)}>Edytuj</button>
      </li>
    );
  }
}

export default BookItem;