import React, { Component } from 'react';
import uuid from 'uuid';

class AddBook extends Component {

  constructor(){
      super();
      this.state = {
          newBook:{}
      }
  }
  static defaultProps = {
      categories: ['Tragedia', 'Powieść']
  }

  handleSubmit(event){
      if(this.refs.title.value === ''){
          alert('Musisz podać tytuł');
      } else {
          this.setState({newBook:{
              id: uuid.v4(),
              title: this.refs.title.value,
              category: this.refs.category.value
          }}, function(){
              this.props.addBook(this.state.newBook);
          })
      }
      event.preventDefault();
  }

  render() {
    let categoryOptions = this.props.categories.map(category => {
        return <option key={category} value={category}>{category}</option>
    });
    return (
      <div>
          <h3>Dodaj Książke</h3>
          <form onSubmit={this.handleSubmit.bind(this)}>
            <div>
                <label>Title:</label>
                <input type="text" ref="title"/>
            </div>
            <div>
                <label>Category:</label>
                <select ref="category">
                {categoryOptions}
                </select>
            </div>
            <br />
            <input type="submit" value="Submit"/>
          </form>
      </div>
    );
  }
}

export default AddBook;