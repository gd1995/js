import React, { Component } from 'react';
import BookItem from './BookItem';

class Books extends Component {
  deleteBook(id){
      this.props.onDelete(id);
  }

  render() {
    let bookItems;
    if(this.props.books){
        bookItems = this.props.books.map(book => {
            return (
                <BookItem onDelete={this.deleteBook.bind(this)} key={book.title} book={book} />
            );
        });
    }
    return (
      <div className="Books">
      <h3>Katalog Książek</h3>
        {bookItems}
      </div>
    );
  }
}

export default Books;