import React, { Component } from 'react';
import uuid from 'uuid';
//import $ from 'jquery';
import Books from './Components/Books';
import AddBook from './Components/AddBook';
import './App.css';

class App extends Component {

  constructor(){
    super();
    this.state = {
      books: []
    }
  }
  /*
  getBooks(){
    $.ajax({
      url: '',
      dataType: 'json',
      cache: false,
      success: function(data){
        this.setState({books: data}, function(){
          console.log(this.state);
        });
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
      }
    });
  }
  */
  getBooks(){
    
    this.setState({books:[
      {
        id: uuid.v4(),
        title: 'Kordian',
        category: 'Tragedia'
      },
      {
        id: uuid.v4(),
        title: 'Quo Vadis',
        category: 'Powieść'
      }
    ]});

  }

  componentWillMount(){
    this.getBooks();
  }

  handleAddBook(book){
    let books = this.state.books;
    books.push(book);
    this.setState({books:books});
  }

  handleDeleteBook(id){
    let books = this.state.books;
    let index = books.findIndex(x => x.id === id);
    books.splice(index, 1);
    this.setState({books:books});
  }

  render() {
    return (
      <div className="App">
      My app
      <AddBook addBook = {this.handleAddBook.bind(this)}/>
      <Books books={this.state.books} onDelete={this.handleDeleteBook.bind(this)}/>
      </div>
    );
  }
}

export default App;
